package com.itexico.chipotlepoc.chipotle.data.interfaces.dao

import android.arch.persistence.room.*
import com.itexico.chipotlepoc.chipotle.data.entitites.Ingredient
import com.itexico.chipotlepoc.chipotle.data.entitites.Meal
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * Created by rdelgado on 6/13/2018.
 */
@Dao
interface MealDao {

    @Query("SELECT * FROM meals")
    fun getAll(): Flowable<List<Meal>>

    @Query("SELECT * FROM meals WHERE id = :id")
    fun getById(id: Int): Single<Meal>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(meal: Meal)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(meal: Meal)

    @Query("DELETE FROM meals")
    fun deleteAll()

    @Delete
    fun delete(meal: Meal)

    @Insert
    fun insertAll(meals: List<Meal>)
}