package com.itexico.chipotlepoc.chipotle.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.itexico.chipotlepoc.chipotle.bussines.ChipotleApplication
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by rdelgado on 5/6/2018.
 */
@Module
abstract class AppModule {

    @Module
    companion object{

        @Provides
        @Singleton
        fun provideSharedPreferences(context: Context): SharedPreferences {
            return context.getSharedPreferences("ChipotlePreferences", Context.MODE_PRIVATE)
        }
    }

    @Binds
    @Singleton
    abstract fun application(app: ChipotleApplication): Application
}