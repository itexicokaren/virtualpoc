package com.itexico.chipotlepoc.chipotle.bussines.interfaces.usecases

import com.itexico.chipotlepoc.common.camera.CameraSourcePreview
import com.itexico.chipotlepoc.common.camera.GraphicOverlay
import com.itexico.chipotlepoc.common.ui.views.FragmentBase

/**
 * Created by rdelgado on 6/6/2018.
 */
interface IFaceDetectionUseCase {

    fun initFaceDetection(fragment: FragmentBase, preview: CameraSourcePreview, faceOverlay: GraphicOverlay)

    fun startCameraSource()

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray)

    fun releaseCamera()

    fun stopPreview()
}