package com.itexico.chipotlepoc.chipotle.ui.views.welcomeChooseMeal

import com.itexico.chipotlepoc.chipotle.ui.views.standBy.StandByFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by diana.munoz on 6/5/2018.
 */
@Module
abstract class StandByFragmentModule {

    @ContributesAndroidInjector
    abstract fun fragment(): StandByFragment
}