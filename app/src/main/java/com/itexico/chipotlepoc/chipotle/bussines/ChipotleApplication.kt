package com.itexico.chipotlepoc.chipotle.bussines

import android.app.Activity
import android.app.Application
import com.itexico.chipotlepoc.chipotle.bussines.interfaces.IChipotleApplication
import com.itexico.chipotlepoc.chipotle.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import javax.inject.Inject

/**
 * Created by rdelgado on 6/5/2018.
 */
class ChipotleApplication : Application(), IChipotleApplication{

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent.builder().application(this).build().inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return activityInjector
    }

}