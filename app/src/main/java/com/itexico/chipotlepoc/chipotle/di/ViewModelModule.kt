package com.itexico.chipotlepoc.chipotle.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.itexico.chipotlepoc.chipotle.ui.viewmodels.*
import com.itexico.chipotlepoc.common.ui.viewmodels.ViewModelFactory
import com.itexico.chipotlepoc.common.ui.viewmodels.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by rdelgado on 6/5/2018.
 */
@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(StandByViewModel::class)
    abstract fun bindStandByViewModel(viewModel: StandByViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WelcomeChooseMealViewModel::class)
    abstract fun bindWelcomeChooseMealViewModel(viewModel: WelcomeChooseMealViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CustomOrderMealsViewModel::class)
    abstract fun bindCustomOrderMealsViewModel(viewModel: CustomOrderMealsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(IngredientsPageViewModel::class)
    abstract fun bindIngredientsPageViewModel(viewModel: IngredientsPageViewModel): ViewModel
}