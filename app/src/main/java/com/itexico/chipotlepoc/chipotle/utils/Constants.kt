package com.itexico.chipotlepoc.chipotle.utils

import com.itexico.chipotlepoc.chipotle.data.entitites.*

/**
 * Created by rdelgado on 5/6/2018.
 */
class Constants{
    companion object {
        const val DATABASE_FILENAME = "chipotle.db"
        const val STANDBY_FACE_RECON_TIME = 3000L

        //database info
        val users = listOf(
                User(1, "Almaz", "111111111"),
                User(2, "Selena", "222222222"),
                User(3, "Sam", "333333333"),
                User(4, "Sammy", "444444444")
        )

        private val filling = IngredientType(0,"fillings")
        private val rice = IngredientType(1,"rice")
        private val bean = IngredientType(2,"beans")
        private val topping = IngredientType(3,"toppings")
        private val side = IngredientType(4,"sides")
        private val drink = IngredientType(5,"drinks")


        val ingredients = listOf(
                Ingredient(1, "chicken","chicken",	1F	,	1f	,	filling	),
                Ingredient(2, "steak","steak",	1F	,	1f	,	filling	),
                Ingredient(3, "carnitas","carnitas",	1F	,	1f	,	filling	),
                Ingredient(4, "barbacoa","barbacoa",	1F	,	1f	,	filling	),
                Ingredient(5, "sofritas","sofritas",	1F	,	1f	,	filling	),
                Ingredient(6, "veggie","fajita_veggies",	1F	,	1f	,	filling	),
                Ingredient(7, "white rice","white_rice",	1F	,	1f	,	rice	),
                Ingredient(8, "brown rice","brown_rice",	1F	,	1f	,	rice	),
                Ingredient(9, "Black beans","black_beans",	1F	,	1f	,	bean	),
                Ingredient(10, "Pinto beans","pinto_beans",	1F	,	1f	,	bean	),
                Ingredient(11, "queso","queso",	1F	,	1f	,	topping	),
                Ingredient(12, "guacamole","guacamole",	1F	,	1f	,	topping	),
                Ingredient(13, "fresh tomato salsa","fresh_tomatoe_salsa",	1F	,	1f	,	topping	),
                Ingredient(14, "roasted chili corn salsa","roasted_chili_corn_salsa",	1F	,	1f	,	topping	),
                Ingredient(15, "tomatillo green chili salsa","tomatillo_green_chili_salsa",	1F	,	1f	,	topping	),
                Ingredient(16, "tomatillo red chili salsa","tomatillo_red_chili_salsa",	1F	,	1f	,	topping	),
                Ingredient(17, "fajita veggies","fajita_veggies",	1F	,	1f	,	topping	),
                Ingredient(18, "sour cream","sour_cream",	1F	,	1f	,	topping	),
                Ingredient(19, "cheese","cheese",	1F	,	1f	,	topping	),
                Ingredient(20, "romaine lettuce","romaine_lettuce",	1F	,	1f	,	topping	),
                Ingredient(21, "chips & queso","chips_and_queso",	1F	,	1f	,	side	),
                Ingredient(22, "large chips & large queso","large_chips_and_large_queso",	1F	,	1f	,	side	),
                Ingredient(23, "side of queso","side_of_queso",	1F	,	1f	,	side	),
                Ingredient(24, "chips & guacamole","chips_and_guacamole",	1F	,	1f	,	side	),
                Ingredient(25, "side of guacamole","side_of_guacamole",	1F	,	1f	,	side	),
                Ingredient(26, "chips & fresh tomatoe salsa","chips_and_fresh_tomatoe_salsa",	1F	,	1f	,	side	),
                Ingredient(27, "chips & roasted chili corn salsa","chips_and_roasted_chili_corn_salsa",	1F	,	1f	,	side	),
                Ingredient(28, "chips & tomatillo red","chips_and_tomatillo_red",	1F	,	1f	,	side	),
                Ingredient(29, "chips & tomatillo green","chips_and_tomatillo_green",	1F	,	1f	,	side	),
                Ingredient(30, "chips","chips",	1F	,	1f	,	side	),
                Ingredient(31, "Pressed apple","pressed_apple",	1F	,	1f	,	drink	),
                Ingredient(32, "peach orange","peach_orange",	1F	,	1f	,	drink	),
                Ingredient(33, "pineapple orange","pineapple_orange",	1F	,	1f	,	drink	),
                Ingredient(34, "pomegranate cherry","pomegranate_cherry",	1F	,	1f	,	drink	),
                Ingredient(35, "clementine","clementine",	1F	,	1f	,	drink	),
                Ingredient(36, "blackberry","blackberry",	1F	,	1f	,	drink	),
                Ingredient(37, "grapefruit","grapefruit",	1F	,	1f	,	drink	),
                Ingredient(38, "22 fl oz soda / iced tea","twenty_two_oz_soda",	1F	,	1f	,	drink	),
                Ingredient(39, "32 fl oz soda / ice tea","thirty_two_oz_soda",	1F	,	1f	,	drink	),
                Ingredient(40, "Bottled water","bottled_water",	1F	,	1f	,	drink	)
        )

        val meals = listOf(
                Meal(1, "Almaz First Meal"),
                Meal(2, "Almaz Second Meal"),
                Meal(3, "Selene First Meal"),
                Meal(4, "Selene Second Meal"),
                Meal(5, "Sam First Meal"),
                Meal(6, "Sam Second Meal"),
                Meal(7, "Samantha First Meal"),
                Meal(8, "Samantha Second Meal")
        )

        val userMeals = listOf(
                UserMealJoin(1,1),
                UserMealJoin(1,2),
                UserMealJoin(2,3),
                UserMealJoin(2,4),
                UserMealJoin(3,5),
                UserMealJoin(3,6),
                UserMealJoin(4,7),
                UserMealJoin(4,8)
        )

        val mealIngredients = listOf(
                MealIngredientJoin(1,1),
                MealIngredientJoin(1,7),
                MealIngredientJoin(1,9),
                MealIngredientJoin(1,11),
                MealIngredientJoin(1,21),
                MealIngredientJoin(1,31),
                MealIngredientJoin(2,4),
                MealIngredientJoin(2,8),
                MealIngredientJoin(2,10),
                MealIngredientJoin(2,12),
                MealIngredientJoin(2,30),
                MealIngredientJoin(2,32)
        )
    }
}