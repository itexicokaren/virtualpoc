package com.itexico.chipotlepoc.chipotle.bussines.usecases

import com.itexico.chipotlepoc.chipotle.bussines.interfaces.usecases.IMealUseCase
import com.itexico.chipotlepoc.chipotle.data.entitites.Ingredient
import com.itexico.chipotlepoc.chipotle.data.entitites.Meal
import com.itexico.chipotlepoc.chipotle.data.interfaces.repositories.IIngredientRepository
import com.itexico.chipotlepoc.chipotle.data.interfaces.repositories.IMealIngredientJoinRepository
import com.itexico.chipotlepoc.chipotle.data.interfaces.repositories.IUserMealJoinRepository
import io.reactivex.Flowable
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by rdelgado on 6/18/2018.
 */
@Singleton
class MealUseCase @Inject constructor(private val ingredientRepo: IIngredientRepository,
                                      private val userMealRepo: IUserMealJoinRepository,
                                      private val mealIngredientRepo: IMealIngredientJoinRepository)
    : IMealUseCase {

    override fun getIngredients(): Flowable<List<Ingredient>> {
        return ingredientRepo.getAll()
    }

    override fun getIngredientsByIngredientType(typeId: Int): Flowable<List<Ingredient>> {
        return ingredientRepo.getIngredientsByType(typeId)
    }

    override fun getMealsByUser(userId: Int): Flowable<List<Meal>>{
        return userMealRepo.getMealsByUser(userId)
    }

    override fun getIngredientsByMeal(mealId: Int): Flowable<List<Ingredient>>{
        return mealIngredientRepo.getIngredientsByMeal(mealId)
    }

    fun getTypeNameByTypeId(typeId: Int): Flowable<List<String>> {
        return ingredientRepo.getTypeNameByTypeId(typeId)
    }

}