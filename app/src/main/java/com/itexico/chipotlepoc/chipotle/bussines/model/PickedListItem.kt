package com.itexico.chipotlepoc.chipotle.bussines.model

import android.databinding.BaseObservable

class PickedListItem(
        var name: String?,
        var listType: String
) : BaseObservable() {
    class TYPE {
        companion object {
            const val HEADER = "header"
            const val HEADER_INACTIVE = "header_inactive"
            const val ITEM = "item"
        }
    }
}
