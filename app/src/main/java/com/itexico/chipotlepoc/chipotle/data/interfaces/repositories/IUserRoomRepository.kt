package com.itexico.chipotlepoc.chipotle.data.interfaces.repositories

import com.itexico.chipotlepoc.chipotle.data.entitites.User
import com.itexico.chipotlepoc.common.data.interfaces.repositories.IRepository
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * Created by rdelgado on 6/19/2018.
 */
interface IUserRoomRepository: IRepository<User> {
    fun getUserByPhone(phone: String): Single<User>
}