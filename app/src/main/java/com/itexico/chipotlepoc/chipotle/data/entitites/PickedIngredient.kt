package com.itexico.chipotlepoc.chipotle.data.entitites

data class PickedIngredient(
        var name: String?,
        var listType: String) {
    class TYPE {
        companion object {
            const val HEADER = "header"
            const val HEADER_INACTIVE = "header_inactive"
            const val ITEM = "item"
        }
    }
}