package com.itexico.chipotlepoc.chipotle.ui.views.login

import android.app.Dialog
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.*
import com.itexico.chipotlepoc.R
import com.itexico.chipotlepoc.chipotle.ui.viewmodels.LoginViewModel
import com.itexico.chipotlepoc.common.ui.views.FragmentBase
import com.itexico.chipotlepoc.databinding.FragmentLoginBinding
import kotlinx.android.synthetic.main.fragment_login.*
import javax.inject.Inject
import android.widget.Toast




/**
 * Created by rdelgado on 6/5/2018.
 */
class LoginFragment: FragmentBase(){

    @Inject
    lateinit var mFactory : ViewModelProvider.Factory

    private lateinit var vm : LoginViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding : FragmentLoginBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_login,
                container,
                false)

        vm = ViewModelProviders
                .of(this, mFactory)
                .get(LoginViewModel::class.java)

        addDisposable(vm.dismiss.subscribe{ _ -> dialog.dismiss()} )

        addDisposable(vm.showMessage.subscribe{msj -> showToast(msj)} )

        binding.context = vm

        return binding.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onStart() {

        editText_telephone_1.showSoftInputOnFocus = false
        editText_telephone_2.showSoftInputOnFocus = false
        editText_telephone_3.showSoftInputOnFocus = false

        val keyboard = phone_keyboard
        keyboard.inputObservable.subscribe{ char -> vm.processInput(char) }

        val dm = DisplayMetrics()

        activity!!.windowManager.defaultDisplay.getMetrics(dm)

        super.onStart()
        val dialog = dialog
        if (dialog != null) {

            dialog.setCanceledOnTouchOutside(false)

            val window = dialog.window
            window!!.setGravity(Gravity.TOP)

            val params = window.attributes
            params.y = 200
            window.attributes = params

            window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            dialog.window!!.setLayout( (dm.widthPixels * .9).toInt() , (dm.heightPixels * .7).toInt())
        }
    }

    fun showToast(msg: String) {
        Toast.makeText(this.context, msg, Toast.LENGTH_SHORT).show()
    }
}