package com.itexico.chipotlepoc.chipotle.data.interfaces.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.itexico.chipotlepoc.chipotle.data.entitites.Ingredient
import com.itexico.chipotlepoc.chipotle.data.entitites.MealIngredientJoin
import io.reactivex.Flowable

/**
 * Created by rdelgado on 6/18/2018.
 */
@Dao
interface MealIngredientJoinDao {

    @Insert(onConflict = REPLACE)
    fun insert(mealIngredientJoin: MealIngredientJoin)

    @Insert(onConflict = REPLACE)
    fun insertAll(userMealJoins: List<MealIngredientJoin>)

    @Query("SELECT * from ingredients INNER JOIN meal_ingredients_join ON id = ingredientId WHERE mealId = :mealId")
    fun getIngredientsByMeal(mealId: Int): Flowable<List<Ingredient>>

}