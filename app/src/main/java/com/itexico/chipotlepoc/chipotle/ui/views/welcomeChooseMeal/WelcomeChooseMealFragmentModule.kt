package com.itexico.chipotlepoc.chipotle.ui.views.welcomeChooseMeal

import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by diana.munoz on 6/11/2018.
 */
@Module
abstract class WelcomeChooseMealFragmentModule {

    @ContributesAndroidInjector
    abstract fun fragment(): WelcomeChooseMealFragment
}