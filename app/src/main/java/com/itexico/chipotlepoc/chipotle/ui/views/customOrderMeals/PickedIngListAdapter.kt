package com.itexico.chipotlepoc.chipotle.ui.views.customOrderMeals

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.itexico.chipotlepoc.R
import com.itexico.chipotlepoc.chipotle.data.entitites.PickedIngredient

/**
 * Created by diana.munoz on 6/18/2018.
 */
class PickedIngListAdapter(private val itemsArray: ArrayList<PickedIngredient>) : RecyclerView.Adapter<PickedIngListAdapter.BindingHolder>() {

    companion object {
        const val TYPE_HEADER = 0
        const val TYPE_HEADER_INACTIVE = 1
        const val TYPE_ITEM = 2
    }

    override fun getItemViewType(position: Int): Int {
        return when (itemsArray[position].listType) {
            PickedIngredient.TYPE.HEADER -> TYPE_HEADER
            PickedIngredient.TYPE.HEADER_INACTIVE -> TYPE_HEADER_INACTIVE
            else -> TYPE_ITEM
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PickedIngListAdapter.BindingHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: com.itexico.chipotlepoc.databinding.ItemPickedItemBinding = DataBindingUtil.inflate(
                layoutInflater,
                R.layout.item_picked_item,
                parent,
                false)
        return PickedIngListAdapter.BindingHolder(binding, viewType, parent.context)
    }

    override fun onBindViewHolder(holder: PickedIngListAdapter.BindingHolder, position: Int) {
        holder.bind(itemsArray[position])
    }

    override fun getItemCount(): Int = itemsArray.size

    class BindingHolder(val binding: com.itexico.chipotlepoc.databinding.ItemPickedItemBinding, val type: Int, val context: Context) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: PickedIngredient) {
            when (type) {
                TYPE_HEADER -> {
                    binding.headerTxt.text = data.name
                    binding.headerTxt.visibility = View.VISIBLE
                    binding.itemTxt.visibility = View.GONE
                }
                TYPE_HEADER_INACTIVE -> {
                    binding.headerTxt.text = data.name
                    binding.headerTxt.setTextColor(ContextCompat.getColor(context, R.color.textPrimaryLight))
                    binding.headerTxt.visibility = View.VISIBLE
                    binding.itemTxt.visibility = View.GONE
                }
                else -> {
                    binding.itemTxt.text = "-${data.name}"
                    binding.headerTxt.visibility = View.GONE
                    binding.itemTxt.visibility = View.VISIBLE
                }
            }

            binding.executePendingBindings()
        }

    }

}