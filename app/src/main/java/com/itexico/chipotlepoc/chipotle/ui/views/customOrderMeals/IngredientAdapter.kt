package com.itexico.chipotlepoc.chipotle.ui.views.customOrderMeals

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.itexico.chipotlepoc.R
import com.itexico.chipotlepoc.chipotle.data.entitites.Ingredient

/**
 * Created by diana.munoz on 6/12/2018.
 */
class IngredientAdapter : RecyclerView.Adapter<IngredientAdapter.BindingHolder>() {

    var binding: com.itexico.chipotlepoc.databinding.ItemIngredientBinding? = null
    var ingredients: List<Ingredient> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IngredientAdapter.BindingHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        binding = DataBindingUtil.inflate(
                layoutInflater,
                R.layout.item_ingredient,
                parent,
                false)
        return IngredientAdapter.BindingHolder(binding!!)
    }

    override fun onBindViewHolder(holder: IngredientAdapter.BindingHolder, position: Int) {
        holder.bind(ingredients[position])
    }

    override fun getItemCount(): Int = ingredients.size



    class BindingHolder(val binding: com.itexico.chipotlepoc.databinding.ItemIngredientBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Ingredient) {
            binding.item = data
            binding.executePendingBindings()
        }
    }

    fun setList(list: List<Ingredient>) {
        this.ingredients = list
    }

}