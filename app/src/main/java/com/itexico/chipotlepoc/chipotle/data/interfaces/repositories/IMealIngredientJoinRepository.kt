package com.itexico.chipotlepoc.chipotle.data.interfaces.repositories

import com.itexico.chipotlepoc.chipotle.data.entitites.Ingredient
import io.reactivex.Flowable

/**
 * Created by rdelgado on 6/18/2018.
 */
interface IMealIngredientJoinRepository {
    fun getIngredientsByMeal(mealId: Int): Flowable<List<Ingredient>>
}