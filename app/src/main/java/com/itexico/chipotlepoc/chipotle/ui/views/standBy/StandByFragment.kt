package com.itexico.chipotlepoc.chipotle.ui.views.standBy

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.itexico.chipotlepoc.R
import com.itexico.chipotlepoc.chipotle.ui.viewmodels.StandByViewModel
import com.itexico.chipotlepoc.common.ui.views.FragmentBase
import com.itexico.chipotlepoc.databinding.FragmentStandByBinding
import kotlinx.android.synthetic.main.fragment_stand_by.*
import javax.inject.Inject

/**
 * Created by rdelgado on 6/5/2018.
 */
class StandByFragment: FragmentBase(){

    @Inject
    lateinit var mFactory : ViewModelProvider.Factory

    private lateinit var vm : StandByViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding : FragmentStandByBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_stand_by,
                container,
                false)

        vm = ViewModelProviders
                .of(this, mFactory)
                .get(StandByViewModel::class.java)

        binding.context = vm

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm.initFaceDetection(this, preview, faceOverlay )
    }

    override fun onResume() {
        super.onResume()
        vm.startCameraSource()
    }

    override fun onPause() {
        super.onPause()
        vm.stopPreview()
    }

    override fun onDestroy() {
        super.onDestroy()
        vm.releaseCamera()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        vm.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

}