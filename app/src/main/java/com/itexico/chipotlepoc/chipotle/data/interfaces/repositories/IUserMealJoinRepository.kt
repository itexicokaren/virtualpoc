package com.itexico.chipotlepoc.chipotle.data.interfaces.repositories

import com.itexico.chipotlepoc.chipotle.data.entitites.Meal
import io.reactivex.Flowable

/**
 * Created by rdelgado on 6/18/2018.
 */
interface IUserMealJoinRepository {
    fun getMealsByUser(userId: Int): Flowable<List<Meal>>
}