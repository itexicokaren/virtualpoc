package com.itexico.chipotlepoc.chipotle.data.entitites

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey

/**
 * Created by rdelgado on 6/18/2018.
 */
@Entity(tableName = "meal_ingredients_join",
        primaryKeys = ["mealId", "ingredientId"],
        foreignKeys = [
            ForeignKey(
                entity = Meal::class,
                parentColumns = ["id"],
                childColumns = ["mealId"]),
            ForeignKey(
                entity = Ingredient::class,
                parentColumns = ["id"],
                childColumns = ["ingredientId"])
            ]
        )
data class MealIngredientJoin(var mealId: Long, var ingredientId: Long)