package com.itexico.chipotlepoc.chipotle.di

import com.itexico.chipotlepoc.chipotle.bussines.interfaces.usecases.IFaceDetectionUseCase
import com.itexico.chipotlepoc.chipotle.bussines.interfaces.usecases.IMealUseCase
import com.itexico.chipotlepoc.chipotle.bussines.interfaces.usecases.IUserUseCase
import com.itexico.chipotlepoc.chipotle.bussines.usecases.FaceDetectionUseCase
import com.itexico.chipotlepoc.chipotle.bussines.usecases.MealUseCase
import com.itexico.chipotlepoc.chipotle.bussines.usecases.UserUseCase
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * Created by rdelgado on 6/6/2018.
 */
@Module
abstract class UseCaseModule {

    @Binds
    @Singleton
    abstract fun faceDetectionUseCase(faceDetectionUseCase: FaceDetectionUseCase): IFaceDetectionUseCase

    @Binds
    @Singleton
    abstract fun userUseCase(userUseCase: UserUseCase): IUserUseCase

    @Binds
    @Singleton
    abstract fun ingredientUseCase(mealUseCase: MealUseCase): IMealUseCase

}