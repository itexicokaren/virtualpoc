package com.itexico.chipotlepoc.chipotle.data.repositories

import com.itexico.chipotlepoc.chipotle.data.databases.ChipotleDatabase
import com.itexico.chipotlepoc.chipotle.data.entitites.Ingredient
import com.itexico.chipotlepoc.chipotle.data.interfaces.repositories.IIngredientRepository
import com.itexico.chipotlepoc.common.rx.SchedulerFacade
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by rdelgado on 6/13/2018.
 */
class IngredientRoomRepository @Inject constructor(chipotleDatabase: ChipotleDatabase)
    : IIngredientRepository {

    private val ingredientDao = chipotleDatabase.ingredientDao()

    override fun getAll(): Flowable<List<Ingredient>> {
        return ingredientDao.getAll()
    }

    override fun getItem(id: Int): Single<Ingredient> {
        return ingredientDao.getById(id)
    }

    override fun add(item: Ingredient) {
        Observable.just<Any>(ingredientDao)
                .subscribeOn(SchedulerFacade.io())
                .subscribe { _ -> ingredientDao.insert(item) }
    }

    override fun update(item: Ingredient) {
        Observable.just<Any>(ingredientDao)
                .subscribeOn(SchedulerFacade.io())
                .subscribe { _ -> ingredientDao.update(item) }
    }

    override fun remove(item: Ingredient) {
        Observable.just<Any>(ingredientDao)
                .subscribeOn(SchedulerFacade.io())
                .subscribe { _ -> ingredientDao.delete(item) }
    }

    override fun removeAll() {
        Observable.just<Any>(ingredientDao)
                .subscribeOn(SchedulerFacade.io())
                .subscribe { _ -> ingredientDao.deleteAll() }
    }

    override fun insertAll(items: List<Ingredient>){
        Observable.just<Any>(ingredientDao)
                .subscribeOn(SchedulerFacade.io())
                .subscribe { _ -> ingredientDao.insertAll(items) }
    }

    override fun getIngredientsByType(typeId: Int): Flowable<List<Ingredient>> {
        return ingredientDao.getIngredientsByType(typeId)
    }

    override fun getTypeNameByTypeId(typeId: Int): Flowable<List<String>> {
        return ingredientDao.getTypesById(typeId)
    }
}