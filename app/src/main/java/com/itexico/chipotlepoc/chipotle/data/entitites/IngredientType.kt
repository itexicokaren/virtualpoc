package com.itexico.chipotlepoc.chipotle.data.entitites

/**
 * Created by rdelgado on 6/13/2018.
 */

data class IngredientType(
        var typeId: Int,
        var typeName: String
)