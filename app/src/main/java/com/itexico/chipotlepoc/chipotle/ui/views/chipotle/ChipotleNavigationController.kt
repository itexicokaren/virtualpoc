package com.itexico.chipotlepoc.chipotle.ui.views.chipotle

import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.util.Log
import com.itexico.chipotlepoc.R
import com.itexico.chipotlepoc.chipotle.ui.views.login.LoginFragment
import com.itexico.chipotlepoc.chipotle.ui.views.customOrderMeals.CustomOrderMealsFragment
import com.itexico.chipotlepoc.chipotle.ui.views.standBy.StandByFragment
import com.itexico.chipotlepoc.chipotle.ui.views.welcomeChooseMeal.WelcomeChooseMealFragment
import com.itexico.chipotlepoc.common.ui.navigation.INavigationManager
import com.itexico.chipotlepoc.common.ui.navigation.INavigationPage
import com.itexico.chipotlepoc.common.ui.views.NavigationControllerBase
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by rdelgado on 6/5/2018.
 */
open class ChipotleNavigationController  @Inject constructor (activity: MainActivity,
                                                              @Named("chipotle") navigationManager: INavigationManager)
    : NavigationControllerBase(navigationManager) {

    companion object {
        private val TAG = ChipotleNavigationController::class.java.simpleName
    }

    private var mFragmentManager: FragmentManager = activity.supportFragmentManager
    private val mContainerId: Int = R.id.chipotle_container

    init {
        navigationManager.navigateToPage(standByPage)
    }

    override fun switchFragment(page: INavigationPage) {

        Log.d(TAG, String.format("navigating to page %d", page.getIndex()))
        when (page.getIndex()) {

            ChipotleNavigationPage.LOGIN ->{
                showFragment(LoginFragment())
                return
            }

            ChipotleNavigationPage.STANDBY -> {
                replaceFragment(StandByFragment())
                return
            }
            ChipotleNavigationPage.WELCOME_CHOOSE_MEAL -> {
                replaceFragment(WelcomeChooseMealFragment())
                return
            }
            ChipotleNavigationPage.CUSTOM_ORDER_MEALS -> {
                replaceFragment(CustomOrderMealsFragment())
                return
            }
            else -> Log.e(TAG, "Unknown navigation page")
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        mFragmentManager
                .beginTransaction()
                .replace(mContainerId, fragment)
                .commitAllowingStateLoss()
    }

    private fun showFragment(fragment: DialogFragment) {
        fragment.show(mFragmentManager, "dialogFrag")
    }

    private val standByPage: INavigationPage
        get() = ChipotleNavigationPage(ChipotleNavigationPage.STANDBY)

}