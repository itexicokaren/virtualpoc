package com.itexico.chipotlepoc.chipotle.ui.views.customOrderMeals

import com.itexico.chipotlepoc.chipotle.ui.views.welcomeChooseMeal.WelcomeChooseMealFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


/**
 * Created by diana.munoz on 6/11/2018.
 */
@Module
abstract class CustomOrderMealsFragmentModule {
    @ContributesAndroidInjector
    abstract fun fragment(): CustomOrderMealsFragment
}