package com.itexico.chipotlepoc.chipotle.ui.viewmodels

import android.databinding.ObservableField
import com.itexico.chipotlepoc.chipotle.bussines.usecases.MealUseCase
import com.itexico.chipotlepoc.common.ui.navigation.INavigationManager
import com.itexico.chipotlepoc.common.ui.viewmodels.ViewModelBase
import javax.inject.Inject
import javax.inject.Named


/**
 * Created by diana.munoz on 6/11/2018.
 */
class CustomOrderMealsViewModel  @Inject constructor(
        @Named("chipotle") private val navigationManager: INavigationManager,
        private val mealUseCase: MealUseCase)
    : ViewModelBase(){

    fun showTitle(typeId: Int) {
//        val titleId = (Math.random() * ((6 - 1) + 1)).toInt()
        val title = "Choose ${mealUseCase.getTypeNameByTypeId(typeId).blockingFirst()[0]}"
        sectionTitle.set(title)
    }

    var sectionTitle = ObservableField<String>("Choose ")
    var typeId = ObservableField<Int>()
}