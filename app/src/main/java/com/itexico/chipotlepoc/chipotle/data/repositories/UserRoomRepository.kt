package com.itexico.chipotlepoc.chipotle.data.repositories

import com.itexico.chipotlepoc.chipotle.data.databases.ChipotleDatabase
import com.itexico.chipotlepoc.chipotle.data.entitites.User
import com.itexico.chipotlepoc.chipotle.data.interfaces.repositories.IUserRoomRepository
import com.itexico.chipotlepoc.common.rx.SchedulerFacade
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by rdelgado on 6/19/2018.
 */
class UserRoomRepository @Inject constructor(chipotleDatabase: ChipotleDatabase) : IUserRoomRepository {

    private val userDao = chipotleDatabase.userDao()

    override fun getAll(): Flowable<List<User>> {
        return userDao.getAll()
    }

    override fun getItem(id: Int): Single<User> {
        return userDao.getById(id)
    }

    override fun add(item: User) {
        Observable.just<Any>(userDao)
                .subscribeOn(SchedulerFacade.io())
                .subscribe { _ -> userDao.insert(item) }
    }

    override fun update(item: User) {
        Observable.just<Any>(userDao)
                .subscribeOn(SchedulerFacade.io())
                .subscribe { _ -> userDao.update(item) }
    }

    override fun remove(item: User) {
        Observable.just<Any>(userDao)
                .subscribeOn(SchedulerFacade.io())
                .subscribe { _ -> userDao.delete(item) }
    }

    override fun removeAll() {
        Observable.just<Any>(userDao)
                .subscribeOn(SchedulerFacade.io())
                .subscribe { _ -> userDao.deleteAll() }
    }

    override fun insertAll(items: List<User>){
        Observable.just<Any>(userDao)
                .subscribeOn(SchedulerFacade.io())
                .subscribe { _ -> userDao.insertAll(items) }
    }

    override fun getUserByPhone(phone: String): Single<User> {
        return userDao.getByPhone(phone)
    }
}