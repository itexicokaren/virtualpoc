package com.itexico.chipotlepoc.chipotle.ui.views.login

import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by rdelgado on 6/5/2018.
 */
@Module
abstract class LoginFragmentModule {

    @ContributesAndroidInjector
    abstract fun fragment(): LoginFragment
}