package com.itexico.chipotlepoc.chipotle.data.entitites

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by rdelgado on 6/18/2018.
 */
@Entity(tableName = "meals")
data class Meal(
        @PrimaryKey(autoGenerate = true) var id: Long?,
        var name: String)