package com.itexico.chipotlepoc.chipotle.data.databases

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.itexico.chipotlepoc.chipotle.data.entitites.*
import com.itexico.chipotlepoc.chipotle.data.interfaces.dao.*
import com.itexico.chipotlepoc.chipotle.utils.Constants
import com.itexico.chipotlepoc.common.rx.SchedulerFacade
import io.reactivex.Observable

/**
 * Created by rdelgado on 6/13/2018.
 */
@Database(entities = [
    Ingredient::class,
    Meal::class,
    User::class,
    MealIngredientJoin::class,
    UserMealJoin::class
    ], version = ChipotleDatabase.VERSION, exportSchema = false )
abstract class ChipotleDatabase : RoomDatabase() {

    companion object {
        const val VERSION = 1
        private val sLock = Any()

        fun getInstance(context: Context?): ChipotleDatabase {

            synchronized(sLock) {
                val instance = Room.databaseBuilder(context!!.applicationContext,
                        ChipotleDatabase::class.java, Constants.DATABASE_FILENAME)
                        .fallbackToDestructiveMigration()
                        .build()

                Observable.just<Any>(instance)
                        .subscribeOn(SchedulerFacade.io())
                        .subscribe { _ -> initDatabase(instance) }

                return instance
            }
        }

        private fun initDatabase(database: ChipotleDatabase) {

            database.ingredientDao().insertAll(Constants.ingredients)

            database.userDao().insertAll(Constants.users)

            database.mealDao().insertAll(Constants.meals)

            database.userMealJoinDao().insertAll(Constants.userMeals)

            database.mealIngredientJoinDao().insertAll(Constants.mealIngredients)

        }
    }

    abstract fun ingredientDao(): IngredientDao

    abstract fun mealDao(): MealDao

    abstract fun userDao(): UserDao

    abstract fun userMealJoinDao(): UserMealJoinDao

    abstract fun mealIngredientJoinDao(): MealIngredientJoinDao

}