package com.itexico.chipotlepoc.chipotle.ui.custom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import com.itexico.chipotlepoc.R
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.numeric_keyboard.view.*

/**
 * Created by rdelgado on 6/12/2018.
 */
class NumericKeyboardView : FrameLayout, View.OnClickListener {

    var inputObservable: BehaviorSubject<CharSequence> = BehaviorSubject.create()

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        View.inflate(context, R.layout.numeric_keyboard, this)
        initViews()
    }

    private fun initViews() {

        t9_key_0.setOnClickListener(this)
        t9_key_1.setOnClickListener(this)
        t9_key_2.setOnClickListener(this)
        t9_key_3.setOnClickListener(this)
        t9_key_4.setOnClickListener(this)
        t9_key_5.setOnClickListener(this)
        t9_key_6.setOnClickListener(this)
        t9_key_7.setOnClickListener(this)
        t9_key_8.setOnClickListener(this)
        t9_key_9.setOnClickListener(this)
        t9_key_backspace.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        if (v.tag != null && "number_button" == v.tag) {
            inputObservable.onNext((v as TextView).text)
            return
        }else{
            inputObservable.onNext("-")
        }
    }
}