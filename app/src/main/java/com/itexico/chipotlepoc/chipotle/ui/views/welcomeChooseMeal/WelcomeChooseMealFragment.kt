package com.itexico.chipotlepoc.chipotle.ui.views.welcomeChooseMeal

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.itexico.chipotlepoc.R
import com.itexico.chipotlepoc.chipotle.ui.viewmodels.WelcomeChooseMealViewModel
import com.itexico.chipotlepoc.common.ui.views.FragmentBase
import com.itexico.chipotlepoc.databinding.FragmentWelcomeChooseMealBinding
import javax.inject.Inject

/**
 * Created by diana.munoz on 6/11/2018.
 */
class WelcomeChooseMealFragment : FragmentBase() {

    @Inject
    lateinit var mFactory : ViewModelProvider.Factory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding : FragmentWelcomeChooseMealBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_welcome_choose_meal,
                container,
                false)

        val vm : WelcomeChooseMealViewModel = ViewModelProviders
                .of(this, mFactory)
                .get(WelcomeChooseMealViewModel::class.java)

        binding.context = vm

        return binding.root
    }
}