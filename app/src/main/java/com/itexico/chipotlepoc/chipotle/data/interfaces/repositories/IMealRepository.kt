package com.itexico.chipotlepoc.chipotle.data.interfaces.repositories

import com.itexico.chipotlepoc.chipotle.data.entitites.Meal
import com.itexico.chipotlepoc.common.data.interfaces.repositories.IRepository

/**
 * Created by rdelgado on 6/13/2018.
 */
interface IMealRepository: IRepository<Meal> {
}