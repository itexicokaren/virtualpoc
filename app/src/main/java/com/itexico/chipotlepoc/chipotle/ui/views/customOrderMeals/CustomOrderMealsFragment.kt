package com.itexico.chipotlepoc.chipotle.ui.views.customOrderMeals

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.itexico.chipotlepoc.R
import com.itexico.chipotlepoc.chipotle.ui.viewmodels.CustomOrderMealsViewModel
import com.itexico.chipotlepoc.chipotle.ui.viewmodels.IngredientsPageViewModel
import com.itexico.chipotlepoc.common.ui.views.FragmentBase
import com.itexico.chipotlepoc.databinding.FragmentCustomOrderMealsBinding
import javax.inject.Inject


/**
 * Created by diana.munoz on 6/11/2018.
 */
class CustomOrderMealsFragment : FragmentBase() {

    @Inject
    lateinit var mFactory : ViewModelProvider.Factory


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding : FragmentCustomOrderMealsBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_custom_order_meals,
                container,
                false)

        val vm : CustomOrderMealsViewModel = ViewModelProviders
                .of(this, mFactory)
                .get(CustomOrderMealsViewModel::class.java)

        binding.context = vm

        binding.pager.adapter = ScreenSlidePagerAdapter(fragmentManager, activity!!.applicationContext)
        binding.tabLayout.setupWithViewPager(binding.pager)
        vm.showTitle(0)
        binding.pager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {
                // Nothing happens
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                if(positionOffset > 0.45F && positionOffset < 0.55F)
                    vm.showTitle(position + 1)

                binding.typeTitle.alpha = when {
                    positionOffset < 0.5    -> 1 - (2 * positionOffset)
                    else -> {
                        (2 * positionOffset) - 1
                    }
               }
            }

            override fun onPageSelected(position: Int) {
                // Nothing happens
            }
        })

        return binding.root
    }
}

class ScreenSlidePagerAdapter(fm: FragmentManager?, val context: Context) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return IngredientsPageFragment.newInstance(position)
    }

    override fun getCount(): Int = 6
}