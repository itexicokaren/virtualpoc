package com.itexico.chipotlepoc.chipotle.data.entitites

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by rdelgado on 6/13/2018.
 */
@Entity(tableName = "ingredients")
data class Ingredient(
        @PrimaryKey(autoGenerate = true) var id: Long?,
        var name: String,
        var image: String,
        var price: Float = 0.0F,
        var calories: Float = 0.0F,
        @Embedded
        var type: IngredientType
)
