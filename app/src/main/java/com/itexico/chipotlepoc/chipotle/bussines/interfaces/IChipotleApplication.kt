package com.itexico.chipotlepoc.chipotle.bussines.interfaces

import dagger.android.HasActivityInjector

/**
 * Created by rdelgado on 6/5/2018.
 */
interface IChipotleApplication : HasActivityInjector {
}