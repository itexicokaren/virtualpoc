package com.itexico.chipotlepoc.chipotle.di

import com.itexico.chipotlepoc.chipotle.ui.views.chipotle.MainActivity
import com.itexico.chipotlepoc.chipotle.ui.views.chipotle.ChipotleActivityModule
import com.itexico.chipotlepoc.common.ui.navigation.INavigationManager
import com.itexico.chipotlepoc.common.ui.navigation.NavigationManager
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by rdelgado on 5/6/2018.
 */
@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = [ChipotleActivityModule::class])
    abstract fun bindMainActivity (): MainActivity

    @Module
    companion object {
        @Provides
        @Named("chipotle")
        @Singleton
        @JvmStatic
        fun bindsNavigationManager(navigationManager: NavigationManager): INavigationManager {
            return navigationManager
        }
    }
}