package com.itexico.chipotlepoc.chipotle.data.interfaces.repositories

import com.itexico.chipotlepoc.chipotle.data.entitites.Ingredient
import com.itexico.chipotlepoc.common.data.interfaces.repositories.IRepository
import io.reactivex.Flowable

/**
 * Created by rdelgado on 6/13/2018.
 */
interface IIngredientRepository: IRepository<Ingredient> {

    fun getIngredientsByType(typeId: Int): Flowable<List<Ingredient>>

    fun getTypeNameByTypeId(typeId: Int): Flowable<List<String>>
}