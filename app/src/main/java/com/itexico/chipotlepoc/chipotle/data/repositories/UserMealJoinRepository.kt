package com.itexico.chipotlepoc.chipotle.data.repositories

import com.itexico.chipotlepoc.chipotle.data.databases.ChipotleDatabase
import com.itexico.chipotlepoc.chipotle.data.entitites.Ingredient
import com.itexico.chipotlepoc.chipotle.data.entitites.Meal
import com.itexico.chipotlepoc.chipotle.data.interfaces.repositories.IUserMealJoinRepository
import io.reactivex.Flowable
import javax.inject.Inject

/**
 * Created by rdelgado on 6/18/2018.
 */
class UserMealJoinRepository @Inject constructor(chipotleDatabase: ChipotleDatabase) : IUserMealJoinRepository {

    private val userMealJoinDao = chipotleDatabase.userMealJoinDao()

    override fun getMealsByUser(userId: Int): Flowable<List<Meal>>{
        return userMealJoinDao.getMealsByUser(userId)
    }
}