package com.itexico.chipotlepoc.chipotle.ui.views.chipotle

import android.app.Activity
import com.itexico.chipotlepoc.chipotle.ui.views.login.LoginFragmentModule
import com.itexico.chipotlepoc.chipotle.ui.views.customOrderMeals.CustomOrderMealsFragmentModule
import com.itexico.chipotlepoc.chipotle.ui.views.customOrderMeals.IngredientsPageFragmentModule
import com.itexico.chipotlepoc.chipotle.ui.views.welcomeChooseMeal.StandByFragmentModule
import com.itexico.chipotlepoc.chipotle.ui.views.welcomeChooseMeal.WelcomeChooseMealFragmentModule
import dagger.Binds
import dagger.Module

/**
 * Created by rdelgado on 6/5/2018.
 */
@Module(includes = [StandByFragmentModule::class, LoginFragmentModule::class, WelcomeChooseMealFragmentModule::class, CustomOrderMealsFragmentModule::class, IngredientsPageFragmentModule::class] )
abstract class ChipotleActivityModule {
    @Binds
    abstract fun bindsActivity(activity: MainActivity): Activity
}