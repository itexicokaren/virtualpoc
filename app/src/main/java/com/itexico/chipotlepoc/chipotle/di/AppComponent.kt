package com.itexico.chipotlepoc.chipotle.di

import com.itexico.chipotlepoc.chipotle.bussines.ChipotleApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by rdelgado on 5/6/2018.
 */
@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    AppModule::class,
    ActivityBindingModule::class,
    ViewModelModule::class,
    UseCaseModule::class,
    DataAccessModule::class
])

interface AppComponent{

    @Component.Builder
    interface Builder
    {
        @BindsInstance
        fun application(application: ChipotleApplication) : Builder

        fun build(): AppComponent
    }

    fun inject (app: ChipotleApplication)
}