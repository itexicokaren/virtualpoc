package com.itexico.chipotlepoc.chipotle.data.repositories

import com.itexico.chipotlepoc.chipotle.data.databases.ChipotleDatabase
import com.itexico.chipotlepoc.chipotle.data.entitites.Ingredient
import com.itexico.chipotlepoc.chipotle.data.interfaces.repositories.IMealIngredientJoinRepository
import io.reactivex.Flowable
import javax.inject.Inject

/**
 * Created by rdelgado on 6/18/2018.
 */
class MealIngredientJoinRepository @Inject constructor(chipotleDatabase: ChipotleDatabase) : IMealIngredientJoinRepository {

    private val mealIngredientJoinDao = chipotleDatabase.mealIngredientJoinDao()

    override fun getIngredientsByMeal(mealId: Int): Flowable<List<Ingredient>>{
        return mealIngredientJoinDao.getIngredientsByMeal(mealId)
    }
}