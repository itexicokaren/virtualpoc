package com.itexico.chipotlepoc.chipotle.data.interfaces.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.itexico.chipotlepoc.chipotle.data.entitites.Meal
import com.itexico.chipotlepoc.chipotle.data.entitites.UserMealJoin
import io.reactivex.Flowable

/**
 * Created by rdelgado on 6/18/2018.
 */
@Dao
interface UserMealJoinDao {

    @Insert(onConflict = REPLACE)
    fun insert(userMealJoin: UserMealJoin)

    @Insert(onConflict = REPLACE)
    fun insertAll(userMealJoins: List<UserMealJoin>)

    @Query("SELECT * from meals INNER JOIN user_meal_join ON id = mealId WHERE userId = :userId")
    fun getMealsByUser(userId: Int): Flowable<List<Meal>>

}