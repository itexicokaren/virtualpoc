package com.itexico.chipotlepoc.chipotle.bussines.adapters

import android.databinding.BindingAdapter
import android.widget.ImageView

object ImageBindingAdapter {
    @JvmStatic
    @BindingAdapter("android:src")
    fun setImageViewResourceByFileName(view: ImageView, fileName: String) {
        val context = view.context
        val resourceId =
                context.resources.getIdentifier(fileName, "drawable", context.packageName )
        view.setImageResource(resourceId)
    }

    @JvmStatic
    @BindingAdapter("android:src")
    fun setImageViewResourceById(view: ImageView, resId: Int) {
        view.setImageResource(resId)
    }
}