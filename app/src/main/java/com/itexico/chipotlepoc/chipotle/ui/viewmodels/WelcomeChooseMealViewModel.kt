package com.itexico.chipotlepoc.chipotle.ui.viewmodels


import android.databinding.ObservableField
import android.util.Log
import com.itexico.chipotlepoc.chipotle.bussines.usecases.UserUseCase
import com.itexico.chipotlepoc.chipotle.data.entitites.User
import com.itexico.chipotlepoc.chipotle.ui.views.chipotle.ChipotleNavigationPage
import com.itexico.chipotlepoc.common.ui.navigation.INavigationManager
import com.itexico.chipotlepoc.common.ui.viewmodels.ViewModelBase
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by diana.munoz on 6/6/2018.
 */
class WelcomeChooseMealViewModel @Inject constructor(
        @Named("chipotle") private val navigationManager: INavigationManager,
        userUseCase: UserUseCase)
        : ViewModelBase() {

    // Unregistered user
    var headerTitle = ObservableField<String>("Hello!")
    var headerSubtitle = ObservableField<String>("Let's get this started")
    var mealsTitle = ObservableField<String>("Choose your favorite")
    var loginInvitation = ObservableField<String>("Login with your Phone Number to access to your personal menu.")
    var loginBtn = ObservableField<String>("Log in")
    var suggestion = ObservableField<String>("Yes, I'm starving")
    //var editMeal = ObservableField<String>("Edit meal")

    var showUnregistered = ObservableField<Boolean>(true)

    init {
        setUnregisteredUI()
        userUseCase.navigateObservable.subscribe{ user -> setLoggedInUI(user)}
    }

    fun navigateToLogin(){
        if(loginBtn.get() == "Log in"){
            navigationManager.navigateToPage(ChipotleNavigationPage(ChipotleNavigationPage.LOGIN))
        }

        if(loginBtn.get() == "Create new"){
            //TODO navigate to correspondent
            Log.d("Keko", "Navigate to new")
        }

    }

    fun navigateToCustomOrder() {
        navigationManager.navigateToPage(ChipotleNavigationPage(ChipotleNavigationPage.CUSTOM_ORDER_MEALS))
    }

    private fun setUnregisteredUI() {
        headerTitle.set("Hello!")
        headerSubtitle.set("Let's get this started")
        mealsTitle.set("Choose your favorite")
        loginBtn.set("Log in")
        showUnregistered.set(true)
    }

    private fun setLoggedInUI(user: User) {
        headerTitle.set("Hello ${user.name}!")
        headerSubtitle.set("Not me")
        mealsTitle.set("You have 5 saved meals")
        loginBtn.set("Create new")
        showUnregistered.set(false)
    }


}