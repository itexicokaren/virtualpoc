package com.itexico.chipotlepoc.chipotle.data.interfaces.dao

import android.arch.persistence.room.*
import com.itexico.chipotlepoc.chipotle.data.entitites.Ingredient
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * Created by rdelgado on 6/13/2018.
 */
@Dao
interface IngredientDao {

    @Query("SELECT * FROM ingredients")
    fun getAll(): Flowable<List<Ingredient>>

    @Query("SELECT * FROM ingredients WHERE id = :id")
    fun getById(id: Int): Single<Ingredient>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(ingredient: Ingredient)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(ingredient: Ingredient)

    @Query("DELETE FROM ingredients")
    fun deleteAll()

    @Delete
    fun delete(ingredient: Ingredient)

    @Insert
    fun insertAll(ingredients: List<Ingredient>)

    @Query("SELECT * FROM ingredients WHERE typeId = :typeId")
    fun getIngredientsByType(typeId: Int): Flowable<List<Ingredient>>

    @Query("SELECT typeName FROM ingredients WHERE typeId = :typeId LIMIT 1")
    fun getTypesById(typeId: Int): Flowable<List<String>>
}