package com.itexico.chipotlepoc.chipotle.ui.views.customOrderMeals

import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by diana.munoz on 6/14/2018.
 */
@Module
abstract class IngredientsPageFragmentModule {
    @ContributesAndroidInjector
    abstract fun fragment(): IngredientsPageFragment
}