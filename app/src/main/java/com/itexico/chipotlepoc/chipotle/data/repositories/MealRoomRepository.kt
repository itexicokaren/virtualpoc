package com.itexico.chipotlepoc.chipotle.data.repositories

import com.itexico.chipotlepoc.chipotle.data.databases.ChipotleDatabase
import com.itexico.chipotlepoc.chipotle.data.entitites.Meal
import com.itexico.chipotlepoc.chipotle.data.interfaces.repositories.IMealRepository
import com.itexico.chipotlepoc.common.rx.SchedulerFacade
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by rdelgado on 6/13/2018.
 */
class MealRoomRepository @Inject constructor(chipotleDatabase: ChipotleDatabase)
    : IMealRepository {

    private val mealDao = chipotleDatabase.mealDao()

    override fun getAll(): Flowable<List<Meal>> {
        return mealDao.getAll()
    }

    override fun getItem(id: Int): Single<Meal> {
        return mealDao.getById(id)
    }

    override fun add(item: Meal) {
        Observable.just<Any>(mealDao)
                .subscribeOn(SchedulerFacade.io())
                .subscribe { _ -> mealDao.insert(item) }
    }

    override fun update(item: Meal) {
        Observable.just<Any>(mealDao)
                .subscribeOn(SchedulerFacade.io())
                .subscribe { _ -> mealDao.update(item) }
    }

    override fun remove(item: Meal) {
        Observable.just<Any>(mealDao)
                .subscribeOn(SchedulerFacade.io())
                .subscribe { _ -> mealDao.delete(item) }
    }

    override fun removeAll() {
        Observable.just<Any>(mealDao)
                .subscribeOn(SchedulerFacade.io())
                .subscribe { _ -> mealDao.deleteAll() }
    }

    override fun insertAll(items: List<Meal>){
        Observable.just<Any>(mealDao)
                .subscribeOn(SchedulerFacade.io())
                .subscribe { _ -> mealDao.insertAll(items) }
    }

}