package com.itexico.chipotlepoc.chipotle.bussines.usecases

import com.itexico.chipotlepoc.chipotle.bussines.interfaces.usecases.IUserUseCase
import com.itexico.chipotlepoc.chipotle.data.entitites.User
import com.itexico.chipotlepoc.chipotle.data.interfaces.repositories.IUserRoomRepository
import com.itexico.chipotlepoc.common.rx.SchedulerFacade
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by rdelgado on 6/11/2018.
 */
@Singleton
class UserUseCase @Inject constructor(private val repository: IUserRoomRepository): IUserUseCase {

    var navigateObservable : BehaviorSubject<User> = BehaviorSubject.create()

    fun getUserNameByPhone(phone: String): Single<User>{
        return repository.getUserByPhone(phone)
    }

    fun getUsers(): Flowable<List<User>>{
        return repository.getAll()
    }
}