package com.itexico.chipotlepoc.chipotle.bussines.interfaces.usecases

import com.itexico.chipotlepoc.chipotle.data.entitites.Ingredient
import com.itexico.chipotlepoc.chipotle.data.entitites.Meal
import io.reactivex.Flowable

/**
 * Created by rdelgado on 6/18/2018.
 */
interface IMealUseCase {

    fun getIngredients(): Flowable<List<Ingredient>>

    fun getIngredientsByIngredientType(typeId: Int): Flowable<List<Ingredient>>

    fun getMealsByUser(userId: Int): Flowable<List<Meal>>

    fun getIngredientsByMeal(mealId: Int): Flowable<List<Ingredient>>
}