package com.itexico.chipotlepoc.chipotle.ui.viewmodels

import android.databinding.ObservableField
import android.util.Log
import com.itexico.chipotlepoc.chipotle.bussines.usecases.MealUseCase
import com.itexico.chipotlepoc.chipotle.bussines.usecases.UserUseCase
import com.itexico.chipotlepoc.common.rx.SchedulerFacade
import com.itexico.chipotlepoc.common.ui.viewmodels.ViewModelBase
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

/**
 * Created by rdelgado on 6/8/2018.
 */
class LoginViewModel @Inject constructor(
        private val userUseCase: UserUseCase,
        mealUseCase: MealUseCase)
    : ViewModelBase() {

    private var completePhoneNumber: StringBuilder = StringBuilder()

    var phoneNumber1 = ObservableField<String>("")
    var phoneNumber2 = ObservableField<String>("")
    var phoneNumber3 = ObservableField<String>("")

    var dismiss : BehaviorSubject<Boolean> = BehaviorSubject.create()

    var showMessage : BehaviorSubject<String> = BehaviorSubject.create()

    init {
        val all = mealUseCase.getIngredients().blockingFirst()
        Log.d("Keko", " Ingredients = $all")

        val toppings = mealUseCase.getIngredientsByIngredientType(4).blockingFirst()
        Log.d("Keko", " toppings = $toppings")

        val users = userUseCase.getUsers().blockingFirst()
        Log.d("Keko", " users = $users")

        val meals = mealUseCase.getMealsByUser(1).blockingFirst()
        Log.d("Keko", " Almaz Meals = $meals")

        val ingredients = mealUseCase.getIngredientsByMeal(1).blockingFirst()
        Log.d("Keko", " Almaz First Meal Ingredients = $ingredients")
    }

    fun loginUser(){

        if(phoneNumber3.get()!!.length < 3){
            showMessage.onNext("Please provide a valid phone number")
        }else{
            userUseCase.getUserNameByPhone(completePhoneNumber.toString())
                    .observeOn(SchedulerFacade.ui())
                    .subscribeOn(SchedulerFacade.io())
                    .subscribe({ user ->
                        dismiss.onNext(true)
                        userUseCase.navigateObservable.onNext(user)
                    }
                            ,{_ ->
                        showMessage.onNext("I'm sorry but we don't have that number registered")})
        }
    }

    fun processInput(c: CharSequence){

        if(c =="-"){
            if(completePhoneNumber.isNotEmpty()){
                completePhoneNumber.deleteCharAt(completePhoneNumber.lastIndex)
            }

            if(completePhoneNumber.length +1 in intArrayOf(1,2,3)){
                phoneNumber1.set(completePhoneNumber.substring(0, completePhoneNumber.length))
            }

            if(completePhoneNumber.length + 1 in intArrayOf(4,5,6)){
                phoneNumber2.set(completePhoneNumber.substring(3, completePhoneNumber.length))
            }

            if(completePhoneNumber.length + 1 in intArrayOf(7,8,9)){
                phoneNumber3.set(completePhoneNumber.substring(6, completePhoneNumber.length))
            }

        }else{

            if(completePhoneNumber.length < 9){
                completePhoneNumber.append(c)
            }

            if(completePhoneNumber.length in intArrayOf(1,2,3)){
                phoneNumber1.set(phoneNumber1.get()+c)
            }

            if(completePhoneNumber.length in intArrayOf(4,5,6)){
                phoneNumber2.set(phoneNumber2.get()+c)
            }

            if(completePhoneNumber.length in intArrayOf(7,8,9)){
                phoneNumber3.set(phoneNumber3.get()+c)
            }
        }
    }
}