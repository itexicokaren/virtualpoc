package com.itexico.chipotlepoc.chipotle.ui.views.chipotle

import android.os.Bundle
import com.itexico.chipotlepoc.R
import com.itexico.chipotlepoc.chipotle.utils.Constants
import com.itexico.chipotlepoc.common.ui.views.ActivityBase
import javax.inject.Inject

/**
 * Created by rdelgado on 6/5/2018.
 */
class MainActivity : ActivityBase() {

    @Inject
    lateinit var mNavigationController: ChipotleNavigationController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(mNavigationController)

        deleteDatabase(Constants.DATABASE_FILENAME)

        setContentView(R.layout.activity_main)
    }
}
