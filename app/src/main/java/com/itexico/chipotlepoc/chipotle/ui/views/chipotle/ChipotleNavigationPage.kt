package com.itexico.chipotlepoc.chipotle.ui.views.chipotle

import android.support.annotation.IntDef
import com.itexico.chipotlepoc.common.ui.navigation.INavigationPage

/**
 * Created by rdelgado on 6/5/2018.
 */
class ChipotleNavigationPage(@ChipotlePage private var index: Int?): INavigationPage {
    companion object {
        const val STANDBY = 0
        const val WELCOME_CHOOSE_MEAL = 1
        const val LOGIN = 2
        const val CUSTOM_ORDER_MEALS = 4
        private val TAG = ChipotleNavigationPage::class.java.simpleName
    }

    override fun getIndex(): Int? {
        return index
    }

    override fun toString(): String {
        return String.format("%s_%d", TAG, getIndex())
    }

    @Retention(AnnotationRetention.SOURCE)
    @IntDef(STANDBY, LOGIN, WELCOME_CHOOSE_MEAL, CUSTOM_ORDER_MEALS)
    annotation class ChipotlePage
}