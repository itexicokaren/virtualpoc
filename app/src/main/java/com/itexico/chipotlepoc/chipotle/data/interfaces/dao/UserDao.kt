package com.itexico.chipotlepoc.chipotle.data.interfaces.dao

import android.arch.persistence.room.*
import com.itexico.chipotlepoc.chipotle.data.entitites.User
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * Created by rdelgado on 6/18/2018.
 */
@Dao
interface UserDao {

    @Query("SELECT * FROM users")
    fun getAll(): Flowable<List<User>>

    @Query("SELECT * FROM users WHERE id = :id")
    fun getById(id: Int): Single<User>

    @Query("SELECT * FROM users WHERE phone = :phone")
    fun getByPhone(phone: String): Single<User>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: User)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(user: User)

    @Query("DELETE FROM users")
    fun deleteAll()

    @Delete
    fun delete(user: User)

    @Insert
    fun insertAll(meals: List<User>)
}