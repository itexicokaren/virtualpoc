package com.itexico.chipotlepoc.chipotle.ui.viewmodels

import android.databinding.BindingAdapter
import android.databinding.ObservableField
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.itexico.chipotlepoc.chipotle.bussines.usecases.MealUseCase
import com.itexico.chipotlepoc.chipotle.data.entitites.Ingredient
import com.itexico.chipotlepoc.chipotle.ui.views.customOrderMeals.IngredientAdapter
import com.itexico.chipotlepoc.common.ui.viewmodels.ViewModelBase
import javax.inject.Inject

/**
 * Created by diana.munoz on 6/14/2018.
 */
@BindingAdapter(*["app:adapter","app:items"])
fun bind(recyclerView: RecyclerView, adapter: IngredientAdapter, items: List<Ingredient>) {
    recyclerView.adapter = adapter
    adapter.setList(items)
}

class IngredientsPageViewModel @Inject constructor(private val mealUseCase: MealUseCase)
    : ViewModelBase(){


    fun showMeTheMoney() {
        val typeId = mTypeId.get()!!
        mItems = mealUseCase.getIngredientsByIngredientType(typeId).blockingFirst()
        Log.d("Karen", " ${mealUseCase.getTypeNameByTypeId(typeId).blockingFirst()} = $mItems")
    }

    var mAdapter = IngredientAdapter()
    var mItems: List<Ingredient> = arrayListOf()
    var mTypeId = ObservableField<Int>()
}
