package com.itexico.chipotlepoc.chipotle.di

import android.app.Application
import com.itexico.chipotlepoc.chipotle.data.databases.ChipotleDatabase
import com.itexico.chipotlepoc.chipotle.data.interfaces.repositories.*
import com.itexico.chipotlepoc.chipotle.data.repositories.*
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by rdelgado on 5/6/2018.
 */
@Module
abstract class DataAccessModule {

    @Module
    companion object{
        @Provides
        @Singleton
        @JvmStatic
        fun provideRoomDatabase(application: Application): ChipotleDatabase {
            return ChipotleDatabase.getInstance(application.applicationContext)
        }
    }

    @Binds
    @Singleton
    internal abstract fun ingredientsRepository(repository: IngredientRoomRepository): IIngredientRepository

    @Binds
    @Singleton
    internal abstract fun mealRepository(repository: MealRoomRepository): IMealRepository

    @Binds
    @Singleton
    internal abstract fun mealIngredientJoinRepository(repository: MealIngredientJoinRepository): IMealIngredientJoinRepository

    @Binds
    @Singleton
    internal abstract fun userMealJoinRepository(repository: UserMealJoinRepository): IUserMealJoinRepository

    @Binds
    @Singleton
    internal abstract fun userRepository(repository: UserRoomRepository): IUserRoomRepository
}