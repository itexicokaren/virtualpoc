package com.itexico.chipotlepoc.chipotle.data.entitites

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey

/**
 * Created by rdelgado on 6/18/2018.
 */
@Entity(tableName = "user_meal_join",
        primaryKeys = ["userId", "mealId"],
        foreignKeys = [
            ForeignKey(
                entity = User::class,
                parentColumns = ["id"],
                childColumns = ["userId"]),
            ForeignKey(
                entity = Meal::class,
                parentColumns = ["id"],
                childColumns = ["mealId"])
        ]
)
class UserMealJoin(var userId: Long, var mealId: Long)
