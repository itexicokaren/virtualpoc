package com.itexico.chipotlepoc.chipotle.ui.views.customOrderMeals

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.itexico.chipotlepoc.R
import com.itexico.chipotlepoc.chipotle.data.entitites.PickedIngredient
import com.itexico.chipotlepoc.chipotle.data.entitites.Ingredient
import com.itexico.chipotlepoc.chipotle.ui.viewmodels.IngredientsPageViewModel
import com.itexico.chipotlepoc.common.ui.views.FragmentBase
import com.itexico.chipotlepoc.databinding.FragmentIngredientsPageBinding
import javax.inject.Inject

/**
 * Created by diana.munoz on 6/12/2018.
 */
class IngredientsPageFragment : FragmentBase() {

    @Inject
    lateinit var mFactory : ViewModelProvider.Factory
    private val mPickedIng : ArrayList<PickedIngredient> = ArrayList()

    companion object {

        private const val EXTRA_ID = "id"

        fun newInstance(typeId: Int): IngredientsPageFragment {
            val fragment = IngredientsPageFragment()
            val args = Bundle()
            args.putInt(EXTRA_ID, typeId)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding : FragmentIngredientsPageBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_ingredients_page,
                container,
                false)

        val vm : IngredientsPageViewModel = ViewModelProviders
                .of(this, mFactory)
                .get(IngredientsPageViewModel::class.java)

        binding.context = vm

        vm.mTypeId.set(arguments?.getInt(EXTRA_ID))

        vm.showMeTheMoney()

        loadPickedIngredients()
        binding.pickedIngRecycler.layoutManager = LinearLayoutManager(context)
        binding.pickedIngRecycler.adapter = PickedIngListAdapter(mPickedIng)

        return binding.root
    }

    private fun loadPickedIngredients() {
        mPickedIng.add(PickedIngredient("Fillings", PickedIngredient.TYPE.HEADER))
        mPickedIng.add(PickedIngredient("Steak", PickedIngredient.TYPE.ITEM))
        mPickedIng.add(PickedIngredient("Carnitas", PickedIngredient.TYPE.ITEM))

        mPickedIng.add(PickedIngredient("Rice", PickedIngredient.TYPE.HEADER))
        mPickedIng.add(PickedIngredient("Brown Rice", PickedIngredient.TYPE.ITEM))

        mPickedIng.add(PickedIngredient("Beans", PickedIngredient.TYPE.HEADER))
        mPickedIng.add(PickedIngredient("Pinto beans", PickedIngredient.TYPE.ITEM))

        mPickedIng.add(PickedIngredient("Toppings", PickedIngredient.TYPE.HEADER))
        mPickedIng.add(PickedIngredient("Fajita Veggies", PickedIngredient.TYPE.ITEM))
        mPickedIng.add(PickedIngredient("Tomatillo Red Chili Salsa", PickedIngredient.TYPE.ITEM))

        mPickedIng.add(PickedIngredient("Sides & drinks", PickedIngredient.TYPE.HEADER_INACTIVE))
        mPickedIng.add(PickedIngredient("Add Ons", PickedIngredient.TYPE.HEADER_INACTIVE))
    }

}