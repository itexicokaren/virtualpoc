package com.itexico.chipotlepoc.chipotle.ui.viewmodels

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.itexico.chipotlepoc.common.ui.navigation.INavigationManager
import com.itexico.chipotlepoc.common.ui.viewmodels.ViewModelBase
import javax.inject.Inject
import javax.inject.Named


/**
 * Created by diana.munoz on 6/21/2018.
 */
@BindingAdapter("android:src")
fun setImageViewResource(view: ImageView, resId: Int) {
    view.setImageResource(resId)
}

class ItemIngredientViewModel @Inject constructor()
    : ViewModelBase(){

}