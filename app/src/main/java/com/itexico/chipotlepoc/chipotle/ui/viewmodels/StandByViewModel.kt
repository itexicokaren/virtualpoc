package com.itexico.chipotlepoc.chipotle.ui.viewmodels

import android.databinding.ObservableField
import com.itexico.chipotlepoc.chipotle.bussines.usecases.FaceDetectionUseCase
import com.itexico.chipotlepoc.chipotle.ui.views.chipotle.ChipotleNavigationPage
import com.itexico.chipotlepoc.common.camera.CameraSourcePreview
import com.itexico.chipotlepoc.common.camera.GraphicOverlay
import com.itexico.chipotlepoc.common.ui.navigation.INavigationManager
import com.itexico.chipotlepoc.common.ui.viewmodels.ViewModelBase
import com.itexico.chipotlepoc.common.ui.views.FragmentBase
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by rdelgado on 6/5/2018.
 */
class StandByViewModel @Inject constructor(
        @Named("chipotle") private val navigationManager: INavigationManager,
        private val faceDetectionUseCase: FaceDetectionUseCase) : ViewModelBase(){

    var showCameraPreview = ObservableField<Boolean>(false)

    lateinit var fragment: FragmentBase

    fun navigateToHome(){
        navigateTo(ChipotleNavigationPage.WELCOME_CHOOSE_MEAL)
    }

    private fun navigateTo(pageId: Int){
        val page = ChipotleNavigationPage(pageId)
        navigationManager.navigateToPage(page)
    }

    fun hideShowCameraPreview(){
        if(showCameraPreview.get()!!){
            showCameraPreview.set(false)
        }else{
            showCameraPreview.set(true)
        }
    }

    fun initFaceDetection(fragment: FragmentBase, preview: CameraSourcePreview, faceOverlay: GraphicOverlay){
        this.fragment = fragment
        faceDetectionUseCase.initFaceDetection(fragment, preview, faceOverlay)
    }

    fun startCameraSource(){
        faceDetectionUseCase.startCameraSource()
        addDisposable(faceDetectionUseCase.navigateObservable.subscribe( { id -> navigateTo(id) } ))
    }


    fun releaseCamera(){
        faceDetectionUseCase.releaseCamera()
    }

    fun stopPreview(){
        faceDetectionUseCase.stopPreview()
    }

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray){
        faceDetectionUseCase.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}