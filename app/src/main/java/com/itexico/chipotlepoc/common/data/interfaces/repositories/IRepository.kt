package com.itexico.chipotlepoc.common.data.interfaces.repositories

import io.reactivex.Flowable
import io.reactivex.Single

/**
 * Created by rdelgado on 5/2/2018.
 */
interface IRepository<T> {

    fun getAll(): Flowable<List<T>>

    fun getItem(id: Int): Single<T>

    fun add(item: T)

    fun update(item: T)

    fun remove(item: T)

    fun removeAll()

    fun insertAll(items: List<T>)
}