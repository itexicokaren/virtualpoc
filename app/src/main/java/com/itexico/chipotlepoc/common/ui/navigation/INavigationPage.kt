package com.itexico.chipotlepoc.common.ui.navigation

/**
 * Created by rdelgado on 6/5/2018.
 */
interface INavigationPage {
    fun getIndex() : Int?
}