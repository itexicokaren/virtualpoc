package com.itexico.chipotlepoc.common.ui.navigation

import io.reactivex.Observable

/**
 * Created by rdelgado on 6/5/2018.
 */
interface INavigationManager {
    fun navigateToPage(page: INavigationPage)

    fun getCurrentPage() : Observable<INavigationPage>
}