package com.itexico.chipotlepoc.common.camera

import com.google.android.gms.vision.MultiProcessor
import com.google.android.gms.vision.Tracker
import com.google.android.gms.vision.face.Face


class GraphicFaceTrackerFactory(overlay: GraphicOverlay) : MultiProcessor.Factory<Face> {

    var mGraphicFaceTracker = GraphicFaceTracker(overlay)

    override fun create(face: Face): Tracker<Face> {
        return mGraphicFaceTracker
    }
}