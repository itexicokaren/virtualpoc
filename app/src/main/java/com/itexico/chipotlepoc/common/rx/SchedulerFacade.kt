package com.itexico.chipotlepoc.common.rx

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by rdelgado on 6/5/2018.
 */
class SchedulerFacade {

    companion object {
        fun io(): Scheduler {
            return Schedulers.io()
        }

        fun computation(): Scheduler {
            return Schedulers.computation()
        }

        fun ui(): Scheduler {
            return AndroidSchedulers.mainThread()
        }
    }

}