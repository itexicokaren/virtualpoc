package com.itexico.chipotlepoc.common.ui.views

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import com.itexico.chipotlepoc.common.rx.SchedulerFacade
import com.itexico.chipotlepoc.common.ui.navigation.INavigationManager
import com.itexico.chipotlepoc.common.ui.navigation.INavigationPage
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by rdelgado on 6/5/2018.
 */
abstract class NavigationControllerBase(private  val navigationManager: INavigationManager) : LifecycleObserver {

    private val disposables = CompositeDisposable()

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun connectListener(){
        disposables.add(navigationManager.getCurrentPage()
                .subscribeOn(SchedulerFacade.io())
                .observeOn(SchedulerFacade.ui())
                .subscribe(this::switchFragment))
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun disconnectListener(){
        disposables.clear()
    }

    internal abstract fun switchFragment(page: INavigationPage)
}