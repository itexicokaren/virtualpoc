package com.itexico.chipotlepoc.common.camera

import android.util.Log
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.Tracker
import com.google.android.gms.vision.face.Face
import com.itexico.chipotlepoc.chipotle.ui.views.chipotle.ChipotleNavigationPage
import com.itexico.chipotlepoc.chipotle.utils.Constants
import io.reactivex.subjects.BehaviorSubject
import java.util.*
import kotlin.concurrent.timerTask


class GraphicFaceTracker internal constructor(private val mOverlay: GraphicOverlay) : Tracker<Face>() {
    private val mFaceGraphic = FaceGraphic(mOverlay)
    private lateinit var timer: Timer

    var navigate: BehaviorSubject<Int> = BehaviorSubject.create<Int>()

    companion object {
        private val TAG = GraphicFaceTracker::class.java.simpleName
    }

    override fun onNewItem(faceId: Int, item: Face?) {
        mFaceGraphic.setId(faceId)
        Log.d(TAG, "Face detected starting timer")
        timer = Timer()
        timer.schedule(timerTask {
            navigate.onNext(ChipotleNavigationPage.WELCOME_CHOOSE_MEAL) }
        , Constants.STANDBY_FACE_RECON_TIME)
    }

    override fun onUpdate(p0: Detector.Detections<Face>?, face: Face?) {
        mOverlay.add(mFaceGraphic)
        face?.let { mFaceGraphic.updateFace(it) }
    }

    override fun onMissing(p0: Detector.Detections<Face>?) {
        mOverlay.remove(mFaceGraphic)
        Log.d(TAG, "Face lost cancel timer")
        timer.cancel()
    }

    override fun onDone() {
        mOverlay.remove(mFaceGraphic)
        Log.d(TAG, "Face done!")
    }
}